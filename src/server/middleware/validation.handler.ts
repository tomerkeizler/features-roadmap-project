import { Request, Response, NextFunction } from "express";
import Ajv, { JTDSchemaType } from "ajv/dist/jtd";

const ajv = new Ajv();

function validationHandler<T>(schema: JTDSchemaType<T>) {
  return (req: Request, res: Response, next: NextFunction) => {

    // validateUser is a type guard for User - type is inferred from schema type
    const validateUser = ajv.compile(schema);
    const valid = validateUser(req.body);

    if (!valid && validateUser.errors) {
      const { instancePath, message } = validateUser.errors[0]
      res.status(422).json({ error: `${instancePath.slice(1)} ${message}` });
      // throw { message: `${instancePath.slice(1)} ${message}` };
    }
    else next();
  };
};

export default validationHandler;
