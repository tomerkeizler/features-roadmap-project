import {RequestHandler, ErrorRequestHandler} from 'express';
import env from "../utils/util.env"

export const error_handler: ErrorRequestHandler = (err, req, res, next) => {
    console.log(err);
    next(err)
}

export const error_handler2: ErrorRequestHandler = (err, req, res, next) => {
    // ORIGINAL
    // if(env('NODE_ENV') !== 'production')res.status(500).json({status:err.message,stack:err.stack});    
    // else res.status(500).json({status:'internal server error...'});

    // SECOND
    let out  = err instanceof Error ? err.message : err;
    if(env('NODE_ENV') !== 'production')res.status(500).send(out);
    else res.status(500).json({status:'internal server error...'});
}

export const not_found: RequestHandler =  (req, res) => {
    console.log(`url: ${req.url} not found...`);
    res.status(404).json({status:`url: ${req.url} not found...`});
}
