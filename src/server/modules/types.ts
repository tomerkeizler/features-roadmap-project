  export interface Feature {
    id?: number;
    status_id: number;
    user_id: number;
    title: string | "";
    content: string | "";
    num_votes: number | 0;
    num_views: number | 0;
    num_comments: number | 0;
  }

  export interface Vote {
    id?: number;
    user_id: number;
    feature_id: number;
  }

  export interface Activity {
    id?: number;
    feature_id: number;
    feature_field_id: number;
    content: string | "";
    time_stamp: string;
  } 

  export interface Feature_status {
    id?: number;
    name: string | "";
    color: string;
  } 

  export interface Feature_field {
    id?: number;
    name: string | "";
  } 
