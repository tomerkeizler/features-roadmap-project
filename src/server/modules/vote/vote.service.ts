import { connection as db } from "../../db/mysql.connection";
import { OkPacket, RowDataPacket } from "mysql2/promise";
import { Vote } from "./vote.type.validate";

export const createVote = async (newVote: Vote): Promise<OkPacket> => {
  const sql = `INSERT INTO vote_practice SET ?`;
  const [result] = await db.query(sql, newVote);
  return result as OkPacket;
};

export const getAllVotes = async (): Promise<Vote[]> => {
  const sql = `SELECT * FROM vote_practice`;
  const [result] = await db.query(sql);
  
  const rows: RowDataPacket[] = result as RowDataPacket[];
  const votes: Vote[] = rows as Vote[];
  return votes;
};

export const getVotesQuantityByUsers = async (): Promise<any[]> => {
    const sql = `
    SELECT u.id, u.name, COUNT(v.id) as num_of_votes
    FROM vote_practice v
    JOIN user u ON v.user_id = u.id
    GROUP BY u.id;`;
    const [result] = await db.query(sql);
  
    const rows: RowDataPacket[] = result as RowDataPacket[];
    return rows;
  }
