import { JTDSchemaType } from "ajv/dist/jtd";

// -------------------------

export interface Vote {
  // id?: number;
  user_id: number;
  text: string;
}

// -------------------------

export const voteSchema: JTDSchemaType<Vote> = {
  // optionalProperties: {
  //   id: { type: "int32" },
  // },
  properties: {
    user_id: { type: "int32" },
    text: { type: "string" },
  },
};
