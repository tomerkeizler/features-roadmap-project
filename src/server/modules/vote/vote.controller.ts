/* 
  if there is an error thrown in the DB, asyncMiddleware
  will pass it to next() and express will handle the error */
  import raw from "../../middleware/route.async.wrapper";
  import express, { Request, Response } from "express";
  import * as voteService from "./vote.service";
  import { Vote, voteSchema } from "./vote.type.validate";
  import validationHandler from "../../middleware/validation.handler"
  
  const router = express.Router();
  
  // parse json req.body on post routes
  router.use(express.json());
  
  // CREATES A NEW VOTE
  router.post(
    "/",
    validationHandler<Vote>(voteSchema),
    raw(async (req: Request, res: Response) => {
      const newVote: Vote = req.body as Vote;
      console.log("create a vote:", newVote);
  
      const result = await voteService.createVote(newVote);
      if (result.affectedRows) {
        res.status(200).json(`Vote was added successfully`);
      } else {
        res.status(404).json({ message: `Error while adding vote` });
      }
    })
  );
  
  // GETS ALL VOTES
  router.get(
    "/",
    raw(async (req: Request, res: Response) => {
      const votes: Vote[] = await voteService.getAllVotes();
      res.status(200).json(votes);
    })
  );
  
  // GETS VOTES QUANTITY BY USERS
router.get(
    "/all",
    raw(async (req: Request, res: Response) => {
      const result = await voteService.getVotesQuantityByUsers();
      res.status(200).json(result);
    })
  );

  export default router;
  