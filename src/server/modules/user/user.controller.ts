/* 
  if there is an error thrown in the DB, asyncMiddleware
  will pass it to next() and express will handle the error */
import raw from "../../middleware/route.async.wrapper";
import express, { Request, Response } from "express";
import * as userService from "./user.service";
import { User, UserUpdate, userSchema, userUpdateSchema } from "./user.type.validate";
import validationHandler from "../../middleware/validation.handler"

const router = express.Router();

// parse json req.body on post routes
router.use(express.json());

// CREATES A NEW USER
router.post(
  "/",
  validationHandler<User>(userSchema),
  raw(async (req: Request, res: Response) => {
    const newUser: User = req.body as User;
    console.log("create a user:", newUser);

    const result = await userService.createUser(newUser);
    if (result.affectedRows) {
      res.status(200).json(`User was added successfully`);
    } else {
      res.status(404).json({ message: `Error while adding user` });
    }
  })
);

// GETS ALL USERS
router.get(
  "/",
  raw(async (req: Request, res: Response) => {
    const users: User[] = await userService.getAllUsers();
    res.status(200).json(users);
  })
);

// GETS ALL USERS - WITH PAGINATION
router.get(
  "/paginate/:page?/:items?",
  raw(async (req: Request, res: Response) => {
    const { page = "0", items = "10" } = req.params;
    const users: User[] = await userService.getPaginatedUsers(parseInt(page), parseInt(items));
    res.status(200).json(users);
  })
);

// GETS A SINGLE USER
router.get(
  "/:id",
  raw(async (req: Request, res: Response) => {
    const userID = parseInt(req.params.id);

    const user = await userService.getSingleUser(userID);
    if (user) {
      res.status(200).json(user);
    } else {
      res.status(404).json({ message: `User ${userID} was not found` });
    }
  })
);

// UPDATES A USER
router.put(
  "/:id",
  validationHandler<UserUpdate>(userUpdateSchema),
  raw(async (req: Request, res: Response) => {
    const userID = parseInt(req.params.id);
    const updatedUser: User = req.body as User;

    const result = await userService.updateUser(userID, updatedUser);
    if (result.affectedRows) {
      res.status(200).json(`User ${userID} was updated successfully`);
    } else {
      res.status(404).json({ message: `User ${userID} was not found` });
    }
  })
);

// DELETES A USER
router.delete(
  "/:id",
  raw(async (req: Request, res: Response) => {
    const userID = parseInt(req.params.id);

    const result = await userService.deleteUser(userID);
    if (result.affectedRows) {
      res.status(200).json(`User ${userID} was deleted successfully`);
    } else {
      res.status(404).json({ message: `User ${userID} was not found` });
    }
  })
);

export default router;
