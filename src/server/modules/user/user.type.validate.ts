import { JTDSchemaType } from "ajv/dist/jtd";

// -------------------------

export interface User {
  // id?: number;
  name: string;
  email: string;
  password: string;
  role: number;
  company_name: string;
  image: string;
}

export interface UserUpdate {
  id?: number;
  name?: string;
  email?: string;
  password?: string;
  role?: number;
  company_name?: string;
  image?: string;
}

// -------------------------

export const userSchema: JTDSchemaType<User> = {
  // optionalProperties: {
  //   id: { type: "int32" },
  // },
  properties: {
    name: { type: "string" },
    email: { type: "string" },
    password: { type: "string" },
    role: { type: "int32" },
    company_name: { type: "string" },
    image: { type: "string" },
  },
};

export const userUpdateSchema: JTDSchemaType<UserUpdate> = {
  optionalProperties: {
    id: { type: "int32" },
    name: { type: "string" },
    email: { type: "string" },
    password: { type: "string" },
    role: { type: "int32" },
    company_name: { type: "string" },
    image: { type: "string" },
  },
};
