import { connection as db } from "../../db/mysql.connection";
import { OkPacket, RowDataPacket } from "mysql2/promise";
import { User } from "./user.type.validate";

export const createUser = async (newUser: User): Promise<OkPacket> => {
  const sql = `INSERT INTO user SET ?`;
  const [result] = await db.query(sql, newUser);
  return result as OkPacket;
};

export const getAllUsers = async (): Promise<User[]> => {
  const sql = `SELECT * FROM user`;
  const [result] = await db.query(sql);
  
  const rows: RowDataPacket[] = result as RowDataPacket[];
  const users: User[] = rows as User[];
  return users;
};

export const getPaginatedUsers = async (page: number, items: number): Promise<User[]> => {
  const sql = `
  SELECT * FROM user
  LIMIT ${items} 
  OFFSET ${page * items}`;
  const [result] = await db.query(sql);

  const rows: RowDataPacket[] = result as RowDataPacket[];
  const users: User[] = rows as User[];
  return users;
}

export const getSingleUser = async (userID: number): Promise<User | null> => {
  const sql = `SELECT * FROM user WHERE id = ?`;
  const [result] = await db.query(sql, userID);

  const row: RowDataPacket = (result as RowDataPacket[])[0];
  const user: User = row as User;
  return user;
}

export const updateUser = async (userID: number, updatedUser: User): Promise<OkPacket> => {
    // UPDATE ALL FIELDS
    // -----------------
    // const { name, email, password, role, company_name, image } = updatedUser;
    // const sql = `
    // UPDATE user SET
    // name = ?,
    // email = ?,
    // password = ?,
    // role = ?,
    // company_name = ?,
    // image = ?
    // WHERE id = ${userID}`;
    // const [result] = await db.query(sql, [name, email, password, role, company_name, image]);
    // return result as OkPacket;


    // CAN ALSO UPDATE PART OF THE FIELDS
    // ----------------------------------
    const updates = Object.entries(updatedUser).map(([key]) => `${key}=?`);
    const sql = `UPDATE user SET ${updates} WHERE id='${userID}'`;
    const [result] = await db.query(sql, Object.values(updatedUser));
    return result as OkPacket;
}

export const deleteUser = async (userID: number): Promise<OkPacket> => {
    const sql = `DELETE FROM user WHERE id = ?`;    
    const [result] = await db.query(sql, [userID]);
    return result as OkPacket;
}
