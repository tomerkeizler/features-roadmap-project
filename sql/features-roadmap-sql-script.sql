CREATE TABLE `feature_status`(
    `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(255) NOT NULL,
    `color` VARCHAR(255) NOT NULL
);
ALTER TABLE
    `feature_status` ADD PRIMARY KEY `feature_status_id_primary`(`id`);
CREATE TABLE `feature`(
    `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
    `status_id` INT UNSIGNED NOT NULL,
    `user_id` INT UNSIGNED NOT NULL,
    `title` VARCHAR(255) NOT NULL,
    `content` VARCHAR(255) NOT NULL,
    `num_votes` INT UNSIGNED NOT NULL,
    `num_views` INT UNSIGNED NOT NULL,
    `num_comments` INT UNSIGNED NOT NULL
);
ALTER TABLE
    `feature` ADD PRIMARY KEY `feature_id_primary`(`id`);
CREATE TABLE `user`(
    `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(255) NOT NULL,
    `email` VARCHAR(255) NOT NULL,
    `password` VARCHAR(255) NOT NULL,
    `company_name` VARCHAR(255) NOT NULL,
    `image` VARCHAR(255) NULL
);
ALTER TABLE
    `user` ADD PRIMARY KEY `user_id_primary`(`id`);
CREATE TABLE `vote`(
    `user_id` INT UNSIGNED NOT NULL,
    `feature_id` INT UNSIGNED NOT NULL
);
CREATE TABLE `activity`(
    `user_id` INT UNSIGNED NOT NULL,
    `feature_id` INT UNSIGNED NOT NULL,
    `feature_field_id` INT UNSIGNED NOT NULL,
    `content` VARCHAR(255) NOT NULL,
    `time_stamp` DATETIME NOT NULL
);
CREATE TABLE `feature_field`(
    `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(255) NOT NULL
);
ALTER TABLE
    `feature_field` ADD PRIMARY KEY `feature_field_id_primary`(`id`);
ALTER TABLE
    `feature` ADD CONSTRAINT `feature_status_id_foreign` FOREIGN KEY(`status_id`) REFERENCES `feature_status`(`id`);
ALTER TABLE
    `feature` ADD CONSTRAINT `feature_user_id_foreign` FOREIGN KEY(`user_id`) REFERENCES `user`(`id`);
ALTER TABLE
    `vote` ADD CONSTRAINT `vote_user_id_foreign` FOREIGN KEY(`user_id`) REFERENCES `user`(`id`);
ALTER TABLE
    `vote` ADD CONSTRAINT `vote_feature_id_foreign` FOREIGN KEY(`feature_id`) REFERENCES `feature`(`id`);
ALTER TABLE
    `activity` ADD CONSTRAINT `activity_user_id_foreign` FOREIGN KEY(`user_id`) REFERENCES `user`(`id`);
ALTER TABLE
    `activity` ADD CONSTRAINT `activity_feature_id_foreign` FOREIGN KEY(`feature_id`) REFERENCES `feature`(`id`);
ALTER TABLE
    `activity` ADD CONSTRAINT `activity_feature_field_id_foreign` FOREIGN KEY(`feature_field_id`) REFERENCES `feature_field`(`id`);